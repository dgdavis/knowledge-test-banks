#!/usr/bin/python3

import os
import sys
import json
import pymongo
from bson import ObjectId
# In order to gain access to modules within the Scripts folder; it shouldn't go beyond scripts
# The first os.path.dirname gets this file's directory; the next one gets its parent
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from shared_local.default_strings import topic_key, question_name_key, group_key, group_name_key, keyword_default, \
    test_banks_path_default, mongo_db_name_default, mongo_host_default, mongo_host_var_name, mongo_port_default, \
    mongo_port_var_name

HOST = os.getenv(mongo_host_var_name, mongo_host_default)
PORT = int(os.getenv(mongo_port_var_name, mongo_port_default))
db_name = mongo_db_name_default
test_banks_path = test_banks_path_default
client = pymongo.MongoClient(HOST, PORT)
db = client[db_name]

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

def write_question_to_file(question:dict, collection:str):
    question_topic = question[topic_key].replace(' ', '_')
    question_name = f'{question[question_name_key]}{keyword_default}'
    if group_key in question:
        question_name = os.path.join(question[group_key][group_name_key], question_name)
    if len(db[collection].distinct(topic_key)) > 1:
        question_name = os.path.join(question_topic, question_name)

    final_path = os.path.join(test_banks_path, collection, question_name)
    os.makedirs(os.path.dirname(final_path), exist_ok=True)

    with open(os.path.abspath(final_path), 'w') as q_file:
        json.dump(question, q_file, indent=4, cls=JSONEncoder)


def main():
    test_bank_dirs = [f.path for f in os.scandir(test_banks_path) if f.is_dir()]
    for test_bank_dir in test_bank_dirs:
        test_bank_name = os.path.basename(test_bank_dir)
        for question in db[test_bank_name].find():
            write_question_to_file(question, test_bank_name)

if __name__ == "__main__":
    main()