#!/usr/bin/python3
import os
import re
import sys
import json
import pymongo
from bson.objectid import ObjectId
# In order to gain access to modules within the Scripts folder; it shouldn't go beyond scripts
# The first os.path.dirname gets this file's directory; the next one gets its parent
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from shared_local.default_strings import question_name_key, rellink_id_key, mongo_db_name_default, \
    mongo_host_default, mongo_host_var_name, mongo_port_default, mongo_port_var_name

HOST = os.getenv(mongo_host_var_name, mongo_host_default)
PORT = int(os.getenv(mongo_port_var_name, mongo_port_default))
client = pymongo.MongoClient(HOST, PORT)
db_mttl = client.mttl
db = client[mongo_db_name_default]

error = False
error_log = {}

def logerror(question: dict, missing_rel_link: bool=False, invalid_rel_link: bool=False):
    global error_log, error
    if missing_rel_link:
        error_log[question[question_name_key]] = 'Missing \'rel-link_id\' field'
    elif invalid_rel_link:
        error_log[question[question_name_key]] = 'Invalid Rel-Link value'
    else:
        error_log[question[question_name_key]] = 'Rel-Link does not exist in MTTL'
    error = True
    return

def main():
    global error_log, error
    collection_list = db.list_collection_names()
    for collection in collection_list:
        for question in db[collection].find({}):
            if rellink_id_key in question and re.match("^[0-9a-f]{24}$", question[rellink_id_key], re.IGNORECASE):
                rel_link = db_mttl.rel_links.find_one({'_id': ObjectId(question[rellink_id_key])})
                if not rel_link: 
                    logerror(question)    
            elif rellink_id_key in question and not re.match("^[0-9a-f]{24}$", question[rellink_id_key], re.IGNORECASE):
                # In case the rel-link's value is something invalid such as an empty string, whitespace, etc.
                logerror(question, invalid_rel_link=True)
            elif rellink_id_key not in question:
                logerror(question, missing_rel_link=True)

    if len(error_log) > 0:
        with open('validate_rel_links.json', 'w') as logfile:
            json.dump(error_log, logfile, indent=4)
    if error:
        exit(1)


if __name__ == "__main__":
    main()