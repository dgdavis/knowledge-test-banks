# 1. INTRODUCTION
Good morning. My name is ___________ and I am from the 90 COS Evaluations flight. I will be administering your exam today. Assisting me is ___________. The exam will begin at the end of this in-brief. You should have no other electronic devices with you except for the examination workstations.

## 1.1 TECHNICAL ISSUES
If you experience any technical difficulties, Direct Message one of the Exam Administrators and you will be taken to another discussion area to solve the issue. You are allowed to ask Exam Administrators questions related to the exam but we will only answer to clarify; we will not provide any information that will assist you with the solution. Don't hesitate to ask questions pertaining to the location of files or tools. 

## 1.2 PHONE NUMBER
When signing up for an Evaluation, you should have entered a phone number we can use to contact you should network communications fail. If you provided a phone number you cannot physically reach during the exam, send a direct message to an Exam Administrator with your updated phone number. Does anyone need to provide an updated phone number?

## 1.3 HEALTH SURVEY
Before you begin the evaluation, fill out the health survey. This will help us understand if you are capable of taking today's evaluation.

# 2. EVALUATION DESCRIPTIONS AND PROCEDURES
This exam you are about to take is a requirement. You must achieve an 80% or better score to be permitted to take the Performance Phase exam. Since this is a “closed book, closed Internet” exam, you are NOT permitted to browse or search the Internet. You are permitted, however, to use the calculator on your phone or computer, if needed.

You will have 90 minutes to answer all multiple choice questions. Remember to take your time and pay attention to the details in the answer options! Make sure you double-check to ensure you've answered all the questions before you submit your exam.

# 3. RESULTS
When your exam is complete, click “Submit.” You will immediately see your score, and any questions you missed along with the correct answers to the missed questions. 

# 4. FEEDBACK
Within one to two business days following this exam, you will receive a short feedback form via e-mail. Please take a few minutes to fill this out so it will help the CCV flight improve their service to the squadron.

# 3. BEGIN EXAMINATION
Are there any final questions?

The time is \<time-hack\>. You may begin your exam.
