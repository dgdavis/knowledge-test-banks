# Table of Contents
- [Test Bank Setup](#test-bank-setup)
  - [Test Bank Data Location](#test-bank-data-location)
  - [Questions](#questions)
    - [File Information](#file-information)
    - [JSON Schema](#json-schema)
    - [Question Templates](#question-templates)
      - [MTTL Rel-link Example](#mttl-rel-link-example)
      - [Normal Question No Snippet Template](#normal-question-no-snippet-template)
      - [Normal Question With Snippet Template](#normal-question-with-snippet-template)
      - [Group Question Template](#group-question-template)
  - [Setup for project BOT](#setup-for-project-bot)
  - [Connecting a MTTL](#connecting-a-mttl)

# Test Bank Setup
This document will help you setup your knowledge test banks.

\[ [TOC](#table-of-contents) \]

## Test Bank Data Location
Within your Knowledge Test Bank, there is a folder called `test-banks`; this is the location where all test bank data for every work-role will be located. Within the `test-banks` folder, you will create a folder for each work-role that requires test bank data. For example, if you had two work-roles called Cyber Capability Developer (CCD) and Product Owner (PO) then you would create a folder for each of these work-roles; the structure would look like the following.

```text
test-banks
|
|- CCD
|
|- PO
```

All [normal](#file-information) questions are located within its respective work-role folder; therefore, CCD questions go in the CCD folder and PO questions in the PO folder. For [group](#file-information) questions, a folder that uses the same name of the group will be located in the CCD or PO folder and all questions belonging to the group will reside within this group folder.

```text
test-banks
|
|- CCD
|  |- loops.question.json
|  |- pointers.question.json
|  |- CONDITIONAL-GROUP
|  |  |- if_statements.question.json
|  |  |- nested_if.question.json
|  |  |- switch_statements.question.json
|  |- error_handling.question.json
|
|- PO
|  |- backlog_refinement.question.json
|  |- sprint_planning.question.json
|  |- sprint_ceremonies.question.json
```

\[ [TOC](#table-of-contents) \]

## Questions
This will explain the structure of question files and the necessary requirement to get a question into your test bank.

\[ [TOC](#table-of-contents) \]

### File Information
Data for a question is stored within a javascript object notation (JSON) file. There may be times when you want to have JSON files that are not meant to be question files; therefore, the extension `.question.json` is used to further deliniate the difference between a question JSON file and other JSON files. Questions have two categories: normal and group. `Normal` questions stand by themselves and do not share the same snippet; whereas, `group` questions all share the same snippet and are organized in the filesystem under a folder using the group name.

\[ [TOC](#table-of-contents) \]

### JSON Schema
A schema is defined that enforces specific key -> value pairs; the schema definition is located in `scripts/test_bank/schemas/knowl_val_schema.json` of your knowledge test bank. In the schema below, the defined keys of the JSON file start in `allOf` (after the `definitions`); some keys are `question`, `snippet`, etc. The expected data type for each key's value is found after the colon. Therefore, the `question` key expects a non-empty string and the `choices` key expects an array of non-empty strings but the array must have a minimum of two items. The line using `$ref` is a way to import a definition that is defined somewhere else; in our case, it is defined in the `definitions` section at the beginning. Definitions are a way to define something once and refer to it multiple times. For more information on JSON schemas and valation of JSON files refer to [json-schema.org](https://json-schema.org/understanding-json-schema/index.html).

- https://gitlab.com/90cos/cyv/eval-systems/scripts/-/blob/master/schemas/definitions.json
- https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/blob/master/scripts/test_bank/schemas/knowl_val_schema.json

\[ [TOC](#table-of-contents) \]

### Question Templates
The information below provides templates that can be used to create new questions. Just create your new question file, naming it something that resembles the main topic being evaluated, and copy/paste the information from the appropriate template below into the file.  

The following information explains each field.
- `rel-link_id`: If using a separate Master Training Task List (MTTL) repository setup like the 90 COS, then the rel-link_id is the OID for the new question's relational data used by the MTTL, see [MTTL Rel-link Example](#mttl-rel-link-example). Otherwise do not include the field.
  - The question's MTTL data will need to be added to the MTTL first so the rel-link_id can be generated and entered into your new question. 
  - Do this by submitting a new issue using the [Add Training Eval Links](https://gitlab.com/90cos/mttl/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) template; note, this link takes you to the 90 COS MTTL new issue page.
- `question_name`: This is the name of the file without the `.question.json` extension.
- `question_path`: This is the relative path of the question's folder; relative to the repository's root folder.
- `question_type`: This identifies the question as a knowledge question, as compared to a performance question.
- `topic`: This identifies the topic in which the question applies.
- `disabled`: This is used by the Exam generator and determines if this question should be allowed in an exam.
- `provisioned`: This is used by the Exam generator and determines the number of times this question was in a generated exam.
- `attempts`: This is a metric that stores the number of times this question was attempted.
- `passes`: This is a metric that stores the number of times this question was answered correctly.
- `failures`: This a metric that stores the number of times this question was answered incorrectly.
- `question`: This is the question itself.
- `snippet`: This is a string representing a code snippet that is part of the question.
- `snippet_lang`: This is the language needed to render the snippet properly.
- `choices`: This is a list of mulitple choice options. 
  - Since this is a list (or array), the index number 0 is the first choice, 1 is the second, and so on.
  - 0=A, 1=B, 2=C, 3=D, etc.
- `answer`: This is the index number to the `choice` that represent the correct answer.
- `explanation`: This is a list of explanations of why a choice is correct or incorrect. You may have one explanation for each choice or one that explains all choices.
- `group`: This identifies this as a group question and stores the group_name and group_questions.
- `group_name`: This is a name for the question group.
- `group_questions`: This is a list of all question `_id` values belonging to the question group.

\[ [TOC](#table-of-contents) \]

#### MTTL Rel-link Example
This demonstrates what an evaluation rel-link looks like for those using an MTTL repository setup like the 90 COS. The data in `_id` -> `$oid` is placed as the value for `rel-link_id` within your `.question.json` file; so, assuming your question is called `quick-sort`, `5ee9250b761cf87371cf315d` would be the value of `rel-link_id`.
```json
{
	"_id": {
		"$oid": "5ee9250b761cf87371cf315d"
	},
	"topic": "Algorithms",
	"KSATs": [
		{
			"ksat_id": {
				"$oid": "5f457f1e1ea90ba9adb32b03"
			},
			"item_proficiency": "B",
			"url": "https://gitlab.com/90cos/cyber-capability-developer-ccd-eval-study/ccd-master-question-file/-/tree/master/knowledge/Algorithms/algorithms-quick-sort/README.md"
		}
	],
	"complexity": 1,
	"revision_number": 1,
	"disabled": false,
	"provisioned": 0,
	"attempts": 0,
	"passes": 0,
	"failures": 0,
	"test_id": "CCD",
	"test_type": "knowledge",
	"question_id": "BD_ALG_0006",
	"network": "commercial",
	"question_name": "quick-sort",
	"language": "3.7",
	"question_proficiency": "B",
	"estimated_time_to_complete": 1,
	"created_on": "2020-06-16",
	"updated_on": "2020-03-13 19:43:08",
	"work-roles": [
		"CCD"
	],
	"map_for": "eval"
}
```

\[ [TOC](#table-of-contents) \]

#### Normal Question No Snippet Template
Use this template for normal questions that do not have a snippet.
```json
{
    "question_name": "",
    "question_path": "",
    "question_type": "knowledge",
    "topic": "",
    "disabled": false,
    "provisioned": 0,
    "attempts": 0,
    "passes": 0,
    "failures": 0,
    "question": "",
    "choices": [
        "",
        "",
        "",
        ""
    ],
    "answer": 1,
    "explanation": [""]
}
```

\[ [TOC](#table-of-contents) \]

#### Normal Question With Snippet Template
Use this template for normal questions that do have a snippet.
```json
{
    "question_name": "",
    "question_path": "",
    "question_type": "knowledge",
    "topic": "",
    "disabled": false,
    "provisioned": 0,
    "attempts": 0,
    "passes": 0,
    "failures": 0,
    "question": "",
    "snippet": "",
    "snippet_lang": "",
    "choices": [
        "",
        "",
        "",
        ""
    ],
    "answer": 0,
    "explanation": [""]
}
```

\[ [TOC](#table-of-contents) \]

#### Group Question Template
Use this template for group questions.
```json
{
    "question_name": "",
    "question_path": "",
    "question_type": "knowledge",
    "topic": "",
    "disabled": false,
    "provisioned": 0,
    "attempts": 0,
    "passes": 0,
    "failures": 0,
    "question": "",
    "snippet": "",
    "snippet_lang": "",
    "choices": [
        "",
        "",
        "",
        ""
    ],
    "answer": 2,
    "explanation": [""],
    "group": {
        "group_name": "",
        "questions": []
    }
}
```

\[ [TOC](#table-of-contents) \]

## Setup for Project BOT

The test bank stages a disposable mongo database in the CI/CD pipeline in some cases. Each question requires a generated `_id` so it can be easily identified by our scripts. A BOT account will be required to make the `_id` field generate automatically for the user on feature branches. If this does not happen, the user would be required to create the `_id` field in the `question.json` files and make sure it is unique. In order to connect a BOT follow these steps.

1. Create a bot GitLab account and add it as a member with `developer` privileges on your forked test-bank.
2. Create an [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) with `read repository` and `write repository` privileges on the BOT account. Save the token for later.
3. Add the following [global runner variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui) on your forked test bank.
  1. Key: `BOT_EMAIL`, value: the email of your BOT account
  2. Key: `BOT_TOKEN`, value: the token generated in step 2
  3. Key: `BOT_USERNAME`, value: the username of the BOT account 

From this point, whenever a new `question.json` is created without the `_id` field, the bot will generate one for the question and commit the changes to the same feature branch.

\[ [TOC](#table-of-contents) \]

## Connecting a MTTL

If you are using a 90COS MTTL and you want to connect it to the test bank. This will allow the questions to list out KSAT information for each question automatically. There are steps required to make this happen. 

1. Create a [global runner variable](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui) with the key of `MTTL_REPO` and the value of the https git clone url. Uncheck the `protected variable` and `mask variable` options.
2. In each `question.json` file, add a `rel-link_id` field with the MTTL rel-link ID. If this does not exist the question will not have a KSAT list.

\[ [TOC](#table-of-contents) \]
