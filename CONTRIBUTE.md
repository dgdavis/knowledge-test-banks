# Contributing
## Submitting contributions
  - [Create a fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) of the repository.
  - Before working please ensure you [create a new branch](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch) and confine your work to that branch
  - When your contribution is ready to be integrated, create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) targeting upstream to the template project. 
  - In the merge request description, detail the changes your contribution made including any bugs fixed, features added, etc.
  - Make sure to `Allow commits from members who can merge to the target branch` before creating the merge request.
  - Ensure your submission passes the CI/CD pipeline.

**NOTE: Consider using [repository mirroring](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository) to ensure your changes are compatible with any subsequent changes, since you will need to pass the CI/CD pipeline upon submission.**

## Contributing Test Material
#### Make the changes yourself
If you want to contribute to test material make sure to manually [run the pipeline](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) in your forked repository. This will create the study presentation mdbook (you can find your projects mdbook url in the repo's `Settings` --> `Pages` view). From here, you can find the questions you want to change and click the `Edit Question` link, which will open a [GitLab WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/) at the specific question's `question.json` file. Make your changes and submit them using the process mentioned above.
#### Create a test question issue
If you do not want to fork and contribute to the test bank directly, you can create a [new issue](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/issues/new?issuable_template=Feedback). Fill it out and the team will colaborate with you to make the changes.

## Suggesting features
  - Create a [new issue](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/issues/new?issuable_template=Feedback)
  - For the title of the issue begin with "SUGGESTION: " followed by a short description of the feature to add.
  - In the description detail the feature's design specifications along with the acceptance criteria for the feature.
  - Submit the issue.
  - Be sure to follow up the issue over time by reading and repsonding to comments, as that will be the primary means of communicating on the progress of the issue.

## Reporting bugs
  - Create a [new issue](https://gitlab.com/90cos/cyv/eval-systems/knowledge-test-bank-system/-/issues/new?issuable_template=Feedback)
  - For the title of the issue begin with "BUG: " followed by a short description of the bug.
  - In the description provide the following:
    - Detailed description of expected behavior.
    - Detailed description of the buggy behavior.
    - As many reproduction steps as you can provide.
  - Submit the issue.
  - Be sure to follow up the issue over time by reading and repsonding to comments, as that will be the primary means of communicating on the progress of the issue.
